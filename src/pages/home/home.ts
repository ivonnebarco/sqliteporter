import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  datos: any = [];
  sectores: any = [];

  constructor(public navCtrl: NavController, private db: DatabaseProvider) {
    // Suscripción para detectar cuándo la BD ha terminado de cargar datos 
    let suscripcionBDPreparada = db.getDatabaseState()
      .subscribe(baseDatosPreparada => {
        if (baseDatosPreparada) {
          // Mantener una referencia a los datos como una propiedad de esta clase
          this.datos = db.datos;
          // Terminar la suscripción
          suscripcionBDPreparada.unsubscribe();
        }
      });
  }


  cargarSectores() {
    this.db.getSectores().then((res) => {
      this.sectores = [];
      for (var i = 0; i < res.rows.length; i++) {
        this.sectores.push({
          pkidtiposector: res.rows.item(i).pkidtiposector,
          nombretiposector: res.rows.item(i).nombretiposector
        });
        
      }

    }, (err) => { /* alert('error al sacar de la bd'+err) */ console.log("error al sacar de la bd " + err) })



  }




}
